package com.rayanmehr.ziyarati.echarge.packageTypeDetails;

import dagger.Subcomponent;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Subcomponent(modules = {
        PackageTypeDetailsModule.class
})
public interface PackagePeriodsSubComponent {

    void inject(PackagePeriodsActivity act);
}
