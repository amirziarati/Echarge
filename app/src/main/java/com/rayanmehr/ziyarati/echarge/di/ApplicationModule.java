package com.rayanmehr.ziyarati.echarge.di;

import android.content.Context;
import android.content.res.Resources;

import com.rayanmehr.ziyarati.echarge.BuildConfig;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.SchedulerProvider;
import com.rayanmehr.ziyarati.echarge.StateManager;
import com.rayanmehr.ziyarati.echarge.tools.AppSchedulerProvider;
import com.rayanmehr.ziyarati.echarge.tools.Consts;
import com.rayanmehr.ziyarati.echarge.tools.StateManagerImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Module
public class ApplicationModule {

    @Provides
    @Singleton
    @Named("isDebug")
    boolean provideIsDebug() {
        return BuildConfig.DEBUG;
    }


    @Provides
    @Singleton
    SchedulerProvider provideAppScheduler() {
        return new AppSchedulerProvider();
    }

    @Provides
    @Singleton
    @Named("networkTimeoutInSeconds")
    int provideNetworkTimeoutInSeconds() {
        return Consts.NETWORK_CONNECTION_TIMEOUT;
    }

    @Provides
    @Singleton
    @Named("cacheMaxAge")
    int provideCacheMaxAgeMinutes() {
        return Consts.CACHE_MAX_AGE;
    }

    @Provides
    @Singleton
    @Named("cacheMaxStale")
    int provideCacheMaxStaleDays() {
        return Consts.CACHE_MAX_STALE;
    }

    @Provides
    @Singleton
    @Named("retryCount")
    int provideApiRetryCount() {
        return Consts.API_RETRY_COUNT;
    }

    @Provides
    @Singleton
    @Named("cacheDir")
    File provideCacheDir(Context context) {
        return context.getCacheDir();
    }

    @Provides
    @Singleton
    @Named("cacheSize")
    long provideCacheSize() {
        return Consts.CACHE_SIZE;
    }

    @Provides
    @Singleton
    HttpUrl provideEndpoint() {
        return HttpUrl.parse(Consts.BASE_URL);
    }

    @Provides
    @Singleton
    StateManager provideStateManager(Context context) {
        return new StateManagerImpl(context);
    }

    @Provides
    @Named("defaultResponse")
    String provideDefaultAppData(Resources resources) {
        InputStream inputStream = resources.openRawResource(R.raw.response);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        String eachline = null;
        StringBuilder result = new StringBuilder("");
        try {
            eachline = bufferedReader.readLine();
            while (eachline != null) {
                result.append(eachline + "\n");
                eachline = bufferedReader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
}
