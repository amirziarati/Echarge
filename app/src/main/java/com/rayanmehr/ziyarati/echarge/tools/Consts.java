package com.rayanmehr.ziyarati.echarge.tools;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class Consts {
    public static final String BASE_URL = "http://www.amirziarati.com/";


    public static final int NETWORK_CONNECTION_TIMEOUT = 30; // 30 sec
    public static final long CACHE_SIZE = 10 * 1024 * 1024; // 10 MB
    public static final int CACHE_MAX_AGE = 2; // 2 min
    public static final int CACHE_MAX_STALE = 30; // 30 day

    public static final int API_RETRY_COUNT = 3;

}
