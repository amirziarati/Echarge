package com.rayanmehr.ziyarati.echarge.operator;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.packageTypes.PackageTypesActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by amir on 2/25/17.
 */
public class OperatorAdapter extends RecyclerView.Adapter<OperatorAdapter.Holder> {

    ArrayList<Operator> lstOperators = new ArrayList<>();
    private Context ctx;
    private LayoutInflater li;

    public OperatorAdapter(Context ctx,ArrayList<Operator> lstOperators) {
        this.lstOperators = lstOperators;
        this.ctx = ctx;
        try {
            li = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Operator getItem(int index)
    {
        return lstOperators.get(index);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = li.inflate(R.layout.list_item_operators, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, PackageTypesActivity.class);
                intent.putExtra( PackageTypesActivity.KEY_ID,getItem(position).getUid());
                ctx.startActivity(intent);
            }
        });
        holder.tvOperator.setText(getItem(position).getTitle());
        Glide.with(ctx).load(getItem(position).getImageUrl()).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                holder.img.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                holder.img.setVisibility(View.VISIBLE);
                return false;
            }
        }).into(holder.img);
        playAnimation(holder.itemView,position);
    }

    private int lastPosition = -1;
    private void playAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(ctx, android.R.anim.slide_in_left);
            animation.setDuration(1500);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return lstOperators.size();
    }

    public class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.imgOperator)
        ImageView img;

        @BindView(R.id.tvOperator)
        TextView tvOperator;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
