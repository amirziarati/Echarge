package com.rayanmehr.ziyarati.echarge.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.db.model.PackageDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackagePeriodDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PreNumber;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class DatabaseHelperImpl extends OrmLiteSqliteOpenHelper implements DatabaseHelper {

    private static final String DATABASE_NAME = "echarge.db";
    private static final int DATABASE_VERSION = 18;

    private Dao<Operator, String> operatorDAO;
    private Dao<PreNumber, Integer> prenumberDAO;
    private Dao<PackageTypeDetails, String> typeDAO;
    private Dao<PackagePeriodDetails, String> periodDAO;
    private Dao<PackageDetails, String> packageDetailsDAO;

    @Inject
    public DatabaseHelperImpl(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION, R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
        try {
            // Create tables. This onCreate() method will be invoked only once of the application life time i.e. the first time when the application starts.
            TableUtils.createTable(connectionSource, Operator.class);
            TableUtils.createTable(connectionSource, PreNumber.class);
            TableUtils.createTable(connectionSource, PackageTypeDetails.class);
            TableUtils.createTable(connectionSource, PackagePeriodDetails.class);
            TableUtils.createTable(connectionSource, PackageDetails.class);

        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Unable to create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
        try {
            TableUtils.dropTable(connectionSource, Operator.class, true);
            TableUtils.dropTable(connectionSource, PreNumber.class, true);
            TableUtils.dropTable(connectionSource, PackageTypeDetails.class, true);
            TableUtils.dropTable(connectionSource, PackagePeriodDetails.class, true);
            TableUtils.dropTable(connectionSource, PackageDetails.class, true);
            onCreate(sqliteDatabase, connectionSource);

        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Unable to upgrade database from version " + oldVer + " to new "
                    + newVer, e);
        }
    }

    public Dao<Operator, String> getOperatorDAO() throws SQLException {
        if (operatorDAO == null) {
            operatorDAO = getDao(Operator.class);
        }
        return operatorDAO;
    }

    public Dao<PreNumber, Integer> getPrenumberDAO() throws SQLException {
        if (prenumberDAO == null) {
            prenumberDAO = getDao(PreNumber.class);
        }
        return prenumberDAO;
    }

    public Dao<PackageTypeDetails, String> getTypeDAO() throws SQLException {
        if (typeDAO == null) {
            typeDAO = getDao(PackageTypeDetails.class);
        }
        return typeDAO;
    }

    public Dao<PackagePeriodDetails, String> getPeriodDAO() throws SQLException {
        if (periodDAO == null) {
            periodDAO = getDao(PackagePeriodDetails.class);
        }
        return periodDAO;
    }

    public Dao<PackageDetails, String> getPackageDetailsDAO() throws SQLException {
        if (packageDetailsDAO == null) {
            packageDetailsDAO = getDao(PackageDetails.class);
        }
        return packageDetailsDAO;
    }

    @Override
    public void close() {
        super.close();
        operatorDAO = null;
    }

    @Override
    public void saveOperator(Operator operator) throws SQLException {

            getOperatorDAO().create(operator);

    }

    @Override
    public void savePreNumber(PreNumber preNumber) throws SQLException {
            getPrenumberDAO().create(preNumber);
    }

    @Override
    public void savePackageType(PackageTypeDetails typesDetail) throws SQLException {
            getTypeDAO().create(typesDetail);
    }

    @Override
    public void savePackagePeriod(PackagePeriodDetails periodsDetail) throws SQLException {
            getPeriodDAO().create(periodsDetail);
    }

    @Override
    public void savePackageDetail(PackageDetails packageDetail) throws SQLException {

            getPackageDetailsDAO().create(packageDetail);

    }

    @Override
    public void recreateAllTables() throws SQLException {

            TableUtils.dropTable(connectionSource, Operator.class, true);
            TableUtils.dropTable(connectionSource, PreNumber.class, true);
            TableUtils.dropTable(connectionSource, PackageTypeDetails.class, true);
            TableUtils.dropTable(connectionSource, PackagePeriodDetails.class, true);
            TableUtils.dropTable(connectionSource, PackageDetails.class, true);

            TableUtils.createTable(connectionSource, Operator.class);
            TableUtils.createTable(connectionSource, PreNumber.class);
            TableUtils.createTable(connectionSource, PackageTypeDetails.class);
            TableUtils.createTable(connectionSource, PackagePeriodDetails.class);
            TableUtils.createTable(connectionSource, PackageDetails.class);

    }

    @Override
    public ArrayList<Operator> getOperators() throws SQLException {
        return new ArrayList<Operator>(getOperatorDAO().queryForAll());
    }

    @Override
    public Operator getOperatorByID(String id) throws SQLException {
        return getOperatorDAO().queryForId(id);
    }

    @Override
    public ArrayList<PackageTypeDetails> getPackageTypes() throws SQLException {
        return new ArrayList<PackageTypeDetails>(getTypeDAO().queryForAll());
    }

    @Override
    public PackageTypeDetails getPackageTypeByID(String id) throws SQLException {
        return getTypeDAO().queryForId(id);
    }

    @Override
    public ArrayList<PackagePeriodDetails> getPackagePeriods() throws SQLException {
        return new ArrayList<PackagePeriodDetails>(getPeriodDAO().queryForAll());
    }

    @Override
    public PackagePeriodDetails getPackagePeriodByID(String id) throws SQLException {
        return getPeriodDAO().queryForId(id);
    }

    @Override
    public ArrayList<PackageDetails> getPackageDetails() throws SQLException {
        return new ArrayList<PackageDetails>(getPackageDetailsDAO().queryForAll());
    }
}
