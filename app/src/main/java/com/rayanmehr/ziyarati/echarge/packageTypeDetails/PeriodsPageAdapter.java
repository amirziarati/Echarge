package com.rayanmehr.ziyarati.echarge.packageTypeDetails;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.rayanmehr.ziyarati.echarge.db.model.PackagePeriodDetails;
import com.rayanmehr.ziyarati.echarge.packagePeriodDetails.PeriodsFragment;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by ziyarati on 2/26/2017.
 */
public class PeriodsPageAdapter extends FragmentPagerAdapter {


    Context ctx;
    ArrayList<PackagePeriodDetails> lst = new ArrayList<>();


    public PeriodsPageAdapter(Context ctx, FragmentManager fragmentManager, ArrayList<PackagePeriodDetails> lst)
    {
        super(fragmentManager);
        this.ctx = ctx;
        this.lst =  lst;
        Collections.reverse(this.lst);
    }

    @Override
    public int getCount() {
        return lst.size();
    }

    @Override
    public Fragment getItem(int position) {
        return PeriodsFragment.newInstance(lst.get(position).getUid() ,position );
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return lst.get(position).getTitle();
    }
}
