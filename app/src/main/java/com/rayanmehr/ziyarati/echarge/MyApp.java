package com.rayanmehr.ziyarati.echarge;

import android.app.Application;

import com.rayanmehr.ziyarati.echarge.di.AndroidModule;
import com.rayanmehr.ziyarati.echarge.di.ApplicationComponent;
import com.rayanmehr.ziyarati.echarge.di.DaggerApplicationComponent;


/**
 * Created by ziyarati on 2/23/2017.
 */

public class MyApp extends Application {

    private ApplicationComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerApplicationComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();

    }

    public ApplicationComponent getAppComponent() {
        return appComponent;
    }
}
