package com.rayanmehr.ziyarati.echarge.di;



import com.rayanmehr.ziyarati.echarge.db.DatabaseModule;
import com.rayanmehr.ziyarati.echarge.operator.OperatorSubComponent;
import com.rayanmehr.ziyarati.echarge.client.ApiModule;
import com.rayanmehr.ziyarati.echarge.client.ClientModule;
import com.rayanmehr.ziyarati.echarge.operators.OperatorModule;
import com.rayanmehr.ziyarati.echarge.packagePeriodDetails.PackagePeriodDetailsModule;
import com.rayanmehr.ziyarati.echarge.packagePeriodDetails.PackagePeriodSubComponent;
import com.rayanmehr.ziyarati.echarge.packageTypeDetails.PackageTypeDetailsModule;
import com.rayanmehr.ziyarati.echarge.packageTypeDetails.PackagePeriodsSubComponent;
import com.rayanmehr.ziyarati.echarge.packageTypes.PackageTypesModule;
import com.rayanmehr.ziyarati.echarge.packageTypes.PackageTypesSubComponent;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Singleton
@Component(modules = {
        AndroidModule.class,
        ApplicationModule.class,
        ApiModule.class,
        ClientModule.class,
        DatabaseModule.class
})
public interface ApplicationComponent {

//    void inject(OperatorActivity activity);

    OperatorSubComponent plus(OperatorModule module);
    PackageTypesSubComponent plus(PackageTypesModule module);
    PackagePeriodsSubComponent plus(PackageTypeDetailsModule module);
    PackagePeriodSubComponent plus(PackagePeriodDetailsModule module);


}