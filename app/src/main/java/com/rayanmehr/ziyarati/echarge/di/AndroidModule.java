package com.rayanmehr.ziyarati.echarge.di;

import android.content.Context;
import android.content.res.Resources;

import com.rayanmehr.ziyarati.echarge.MyApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Module
public class AndroidModule {


    private final MyApp application;


    public AndroidModule(final MyApp application) {
        this.application = application;
    }


    @Provides
    @Singleton
    Context provideContext() {
        return application.getApplicationContext();
    }


    @Provides
    @Singleton
    Resources provideResources() {
        return application.getResources();
    }

}
