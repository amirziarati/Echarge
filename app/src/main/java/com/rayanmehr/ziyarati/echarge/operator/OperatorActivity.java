package com.rayanmehr.ziyarati.echarge.operator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rayanmehr.ziyarati.echarge.MyApp;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.DatabaseHelper;
import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.operators.OperatorModule;
import com.rayanmehr.ziyarati.echarge.operators.OperatorsContract;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;



public class OperatorActivity extends AppCompatActivity implements OperatorsContract.View {


    @Inject
    OperatorsContract.Presenter operatorPresenter;

    @BindView(R.id.lstOperators)
    RecyclerView lstOp;

    @BindView(R.id.prg)
    ProgressBar prg;

    @Inject
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operators);
        ButterKnife.bind(this);
        ((MyApp) getApplication()).getAppComponent().plus(new OperatorModule(this)).inject(this);
//        try {
//            new hardcodes(databaseHelper);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
        lstOp.setLayoutManager(new LinearLayoutManager(this));
        operatorPresenter.getAllOperators();
    }

    @Override
    public void populateView(ArrayList<Operator> operator) {
        lstOp.setAdapter(new OperatorAdapter(this,operator));
    }

    @Override
    public void showProgress() {
        prg.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        prg.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showOfflineMessage(boolean isCritical) {

    }
}
