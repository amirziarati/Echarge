package com.rayanmehr.ziyarati.echarge.packagePeriodDetails;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rayanmehr.ziyarati.echarge.BuildConfig;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.db.model.PackageDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PreNumber;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ziyarati on 2/26/2017.
 */

public class ConfirmDialog extends Dialog {

    @BindView(R.id.btnPurchase)
    Button btnPurchase;

    @BindView(R.id.txtCardNumber)
    EditText txtCardNumber;

    @BindView(R.id.tvPackageSize)
    TextView tvPackageSize;

    @BindView(R.id.tvPackagePeriod)
    TextView tvPackagePeriod;

    @BindView(R.id.tvPackageType)
    TextView tvPackageType;

    @BindView(R.id.tvPriceValue)
    TextView tvPriceValue;

    @BindView(R.id.tvTaxValue)
    TextView tvTaxValue;

    @BindView(R.id.tvSum)
    TextView tvSum;

    @BindView(R.id.txtCell)
    EditText txtCell;

    Activity ctx;
    PackageDetails packageDetails;

    public ConfirmDialog(Activity ctx, PackageDetails packageDetails) {
        super(ctx);
        this.ctx = ctx;
        this.packageDetails = packageDetails;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dlg_confirm_purchase);
        ButterKnife.bind(this);
        txtCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        tvPackageSize.setText(packageDetails.getPackageSize());
        tvPackagePeriod.setText(packageDetails.getPackageDuration());
        tvPriceValue.setText(packageDetails.getPrice());
        tvPackageType.setText(packageDetails.getPackagePeriodDetails().getPackageTypeDetails().getTitle());

        final int price = Integer.parseInt(packageDetails.getPrice());
        int tax = (int) (price * 0.09);
        int sum = price + tax;
        tvTaxValue.setText(tax + "");
        tvSum.setText(sum + "");

        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Operator operator = packageDetails.getPackagePeriodDetails().getPackageTypeDetails().getOperator();
                ArrayList<PreNumber> lstPreNumbers = new ArrayList<PreNumber>();
                if (operator.getTitle().equals(ctx.getString(R.string.irancell_value))) {
                    lstPreNumbers = operator.getPreNumbers();
                } else if (operator.getTitle().equals(ctx.getString(R.string.mci_value))) {
                    lstPreNumbers = operator.getPreNumbers();
                } else if (operator.getTitle().equals(ctx.getString(R.string.rightel_value))) {
                    lstPreNumbers = operator.getPreNumbers();
                }
                String phone = txtCell.getText().toString();

                boolean isValidNum = false;
                if (isAnIranianValidPhoneNumber(phone)) {
                    //TODO: write three regex for each operator instead of this
                    for (PreNumber preNumber : lstPreNumbers) {
                        if (phone.startsWith("0" + preNumber.getNumber()) ||
                                phone.startsWith("0098" + preNumber.getNumber()) ||
                                phone.startsWith("+98" + preNumber.getNumber()) ||
                                phone.startsWith("98" + preNumber.getNumber()) ||
                                phone.startsWith(preNumber.getNumber())) {
                            isValidNum = true;
                            break;
                        }
                    }

                } else {
                    Toast.makeText(ctx, "شماره وارد شده صحیح نیست !", Toast.LENGTH_SHORT).show();

                }

                if (!isValidNum) {
                    Toast.makeText(ctx, "شماره وارد شده مربوط به این اپراتور نیست !", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (txtCardNumber.getText().toString().length() != 19) {
                    Toast.makeText(ctx, "شماره کارت وارد شده صحیح نمی باشد !", Toast.LENGTH_SHORT).show();
                    return;
                }

                String productID = "1";
                String ussd = makeUssdString("*789*1*1", phone, productID, price, txtCardNumber.getText().toString(), BuildConfig.VERSION_CODE);
                uri = ussdToCallableUri(ussd);
                if (android.os.Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(ctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    dismiss();
                    ActivityCompat.requestPermissions(ctx, new String[]{Manifest.permission.CALL_PHONE}, 1);
                    Toast.makeText(ctx, "برای شارژ نیاز به مجوز تماس می باشد دوباره تلاش کنید !", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    startCallActivity(ctx);
                }
            }
        });

    }

    //TODO: this part is better to be a seperate static class
    static Uri uri;
    public static void startCallActivity(Activity context) {
        Intent callIntent = new Intent(Intent.ACTION_CALL, uri);
        context.startActivity(callIntent);
    }

    private boolean isAnIranianValidPhoneNumber(String number) {
        boolean isValid = false;

        String expression = "(0|\\+98)?([ ]|-|[()]){0,2}9[1|2|3|4]([ ]|-|[()]){0,2}(?:[0-9]([ ]|-|[()]){0,2}){8}";
        CharSequence inputStr = number;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    private Uri ussdToCallableUri(String ussd) {

        String uriString = "";

        if (!ussd.startsWith("tel:"))
            uriString += "tel:";

        for (char c : ussd.toCharArray()) {

            if (c == '#')
                uriString += Uri.encode("#");
            else
                uriString += c;
        }

        return Uri.parse(uriString);
    }

    private String makeUssdString(String preUssdNumber, String phone, String productid, int price, String cardNumber, int versionCode) {
        if (phone.startsWith("+989"))
            phone = phone.substring(4, phone.length());
        else if (phone.startsWith("989"))
            phone = phone.substring(3, phone.length());
        else if (phone.startsWith("09"))
            phone = phone.substring(2, phone.length());
        else if (phone.startsWith("9"))
            phone = phone.substring(1, phone.length());


        price = price / 1000;

        cardNumber = cardNumber.replaceAll("-", "");

        return preUssdNumber + "*" + phone + "*" + productid + "*" + price + "*" + cardNumber + "*" + "1" + "*" + versionCode + "#";
    }

    public class FourDigitCardFormatWatcher implements TextWatcher {

        private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
        private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
        private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
        private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
        private static final char DIVIDER = '-';

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            // noop
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // noop
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
                s.replace(0, s.length(), buildCorrecntString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
            }
        }

        private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
            boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
            for (int i = 0; i < s.length(); i++) { // chech that every element is right
                if (i > 0 && (i + 1) % dividerModulo == 0) {
                    isCorrect &= divider == s.charAt(i);
                } else {
                    isCorrect &= Character.isDigit(s.charAt(i));
                }
            }
            return isCorrect;
        }

        private String buildCorrecntString(char[] digits, int dividerPosition, char divider) {
            final StringBuilder formatted = new StringBuilder();

            for (int i = 0; i < digits.length; i++) {
                if (digits[i] != 0) {
                    formatted.append(digits[i]);
                    if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                        formatted.append(divider);
                    }
                }
            }

            return formatted.toString();
        }

        private char[] getDigitArray(final Editable s, final int size) {
            char[] digits = new char[size];
            int index = 0;
            for (int i = 0; i < s.length() && index < size; i++) {
                char current = s.charAt(i);
                if (Character.isDigit(current)) {
                    digits[index] = current;
                    index++;
                }
            }
            return digits;
        }
    }
}
