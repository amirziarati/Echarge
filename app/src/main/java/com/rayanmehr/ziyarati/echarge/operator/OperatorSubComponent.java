package com.rayanmehr.ziyarati.echarge.operator;

import com.rayanmehr.ziyarati.echarge.operators.OperatorModule;

import dagger.Subcomponent;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Subcomponent(modules = {
        OperatorModule.class
})
public interface OperatorSubComponent {

    void inject(OperatorActivity act);
}
