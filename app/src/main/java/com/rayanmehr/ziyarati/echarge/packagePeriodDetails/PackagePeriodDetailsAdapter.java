package com.rayanmehr.ziyarati.echarge.packagePeriodDetails;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.PackageDetails;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ziyarati on 2/26/2017.
 */
public class PackagePeriodDetailsAdapter extends RecyclerView.Adapter<PackagePeriodDetailsAdapter.Holder>  {
    ArrayList<PackageDetails> lstOperators = new ArrayList<>();
    String title = "";
    private Activity ctx;
    private LayoutInflater li;


    public PackagePeriodDetailsAdapter(Activity ctx, ArrayList<PackageDetails> lstOperators, String title) {
        this.lstOperators = lstOperators;
        this.ctx = ctx;
        this.title = title;
        try {
            li = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PackageDetails getItem(int index) {
        return lstOperators.get(index);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = li.inflate(R.layout.list_item_package_detail, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    ConfirmDialog dialog = new ConfirmDialog(ctx, getItem(position));
                    dialog.show();
            }
        });
        holder.tvPackagePrice.setText(getItem(position).getPrice());
        holder.tvPackageSize.setText(getItem(position).getPackageSize());
        holder.tvPackageDiscription.setText(getItem(position).getPackageDescription());
    }


    @Override
    public int getItemCount() {
        return lstOperators.size();
    }

    public class Holder extends RecyclerView.ViewHolder {


        @BindView(R.id.tvPackagePrice)
        TextView tvPackagePrice;

        @BindView(R.id.tvPackageSize)
        TextView tvPackageSize;

        @BindView(R.id.tvPackageDescription)
        TextView tvPackageDiscription;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
