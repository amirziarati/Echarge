package com.rayanmehr.ziyarati.echarge.db;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Module
public class DatabaseModule {

    @Provides
    @Singleton
    DatabaseHelper provideDatabaseHelperService(DatabaseHelperImpl databaseHelper) {
        return databaseHelper;
    }

}
