package com.rayanmehr.ziyarati.echarge.packageTypeDetails;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rayanmehr.ziyarati.echarge.MyApp;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.packagePeriodDetails.ConfirmDialog;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PackagePeriodsActivity extends AppCompatActivity implements PackageTypeDetailsContract.View, ActivityCompat.OnRequestPermissionsResultCallback {


    public static final String KEY_ID = "id";
    @Inject
    PackageTypeDetailsContract.Presenter packageTypeContract;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tab_header)
    TabLayout tab_header;


    @BindView(R.id.prg)
    ProgressBar prg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_type_details);
        ButterKnife.bind(this);

        ((MyApp) getApplication()).getAppComponent().plus(new PackageTypeDetailsModule(this)).inject(this);

        tab_header.setupWithViewPager(viewPager);
        packageTypeContract.getPackageTypeDetails(getIntent().getExtras().getString(KEY_ID));
    }


    @Override
    public void populateView(PackageTypeDetails packageTypeDetails) {
        viewPager.setAdapter(new PeriodsPageAdapter(this, getSupportFragmentManager(), packageTypeDetails.getPackagePeriods()));
        viewPager.setCurrentItem(packageTypeDetails.getPackagePeriods().size() - 1);
    }

    @Override
    public void showProgress() {
        prg.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        prg.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showOfflineMessage(boolean isCritical) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (android.os.Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            ConfirmDialog.startCallActivity(this);
        }
    }
}
