package com.rayanmehr.ziyarati.echarge.packagePeriodDetails;

import dagger.Subcomponent;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Subcomponent(modules = {
        PackagePeriodDetailsModule.class
})
public interface PackagePeriodSubComponent {

    void inject(PeriodsFragment act);
}
