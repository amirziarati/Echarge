package com.rayanmehr.ziyarati.echarge.packageTypes;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.rayanmehr.ziyarati.echarge.MyApp;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


//TODO:
//operators list UI
//packageType UI
//PackagePeriod And lists UI
//Confirm Dialog UI
public class PackageTypesActivity extends AppCompatActivity implements PackageTypesContract.View {


    public static final String KEY_ID = "id";

    @Inject
    PackageTypesContract.Presenter packageTypeContract;

    @BindView(R.id.lstPackageTypes)
    RecyclerView lstPackageTypes;

    @BindView(R.id.prg)
    ProgressBar prg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_package_types);
        ButterKnife.bind(this);

        ((MyApp) getApplication()).getAppComponent().plus(new PackageTypesModule(this)).inject(this);

        lstPackageTypes.setLayoutManager(new LinearLayoutManager(this));


        packageTypeContract.getPackageTypes(getIntent().getExtras().getString(KEY_ID));
    }


    @Override
    public void populateView(ArrayList<PackageTypeDetails> packageTypes) {
        lstPackageTypes.setAdapter(new PackageTypesAdapter(this, packageTypes));
    }


    @Override
    public void showProgress() {
        prg.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        prg.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showOfflineMessage(boolean isCritical) {

    }
}
