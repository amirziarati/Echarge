package com.rayanmehr.ziyarati.echarge.packageTypes;

import dagger.Subcomponent;

/**
 * Created by ziyarati on 2/23/2017.
 */

@Subcomponent(modules = {
        PackageTypesModule.class
})
public interface PackageTypesSubComponent {

    void inject(PackageTypesActivity act);
}
