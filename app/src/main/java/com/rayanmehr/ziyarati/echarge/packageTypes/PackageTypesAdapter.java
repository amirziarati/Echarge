package com.rayanmehr.ziyarati.echarge.packageTypes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.packageTypeDetails.PackagePeriodsActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by amir on 2/25/17.
 */
public class PackageTypesAdapter extends RecyclerView.Adapter<PackageTypesAdapter.Holder> {

    ArrayList<PackageTypeDetails> lstOperators = new ArrayList<>();
    private Context ctx;
    private LayoutInflater li;

    public PackageTypesAdapter(Context ctx, ArrayList<PackageTypeDetails> lstOperators) {
        this.lstOperators = lstOperators;
        this.ctx = ctx;
        try {
            li = (LayoutInflater) this.ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private PackageTypeDetails getItem(int index)
    {
        return lstOperators.get(index);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = li.inflate(R.layout.list_item_package_types, parent, false);
        Holder holder = new Holder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, PackagePeriodsActivity.class);
                intent.putExtra(PackagePeriodsActivity.KEY_ID,getItem(position).getUid());
                ctx.startActivity(intent);
            }
        });
        holder.tvPackageType.setText(getItem(position).getTitle());
        playAnimation(holder.itemView,position);
    }

    private int lastPosition = -1;
    private void playAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(ctx, android.R.anim.fade_in);
            animation.setDuration(5000);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return lstOperators.size();
    }

    public class Holder extends RecyclerView.ViewHolder{



        @BindView(R.id.tvPackageType)
        TextView tvPackageType;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
