package com.rayanmehr.ziyarati.echarge.packagePeriodDetails;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.rayanmehr.ziyarati.echarge.MyApp;
import com.rayanmehr.ziyarati.echarge.R;
import com.rayanmehr.ziyarati.echarge.db.model.PackagePeriodDetails;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ziyarati on 2/26/2017.
 */
public class PeriodsFragment extends Fragment implements PackagePeriodDetailsContract.View  {

    public static final String KEY_ID = "id";
    public static final String KEY_PAGE = "page";

    @Inject
    PackagePeriodDetailsContract.Presenter presenter;

    @BindView(R.id.lstPackageDetails)
    RecyclerView lstPackageDetails;

    private String id;
    private int page;

    public static PeriodsFragment newInstance(String id, int page) {
        PeriodsFragment fragmentFirst = new PeriodsFragment();
        Bundle args = new Bundle();
        args.putInt(KEY_PAGE, page);
        args.putString(KEY_ID, id);

        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MyApp) getActivity().getApplication()).getAppComponent().plus(new PackagePeriodDetailsModule(this)).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frg_item_package_period, container, false);
        ButterKnife.bind(this, view);
        lstPackageDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
        page = getArguments().getInt(KEY_PAGE, 0);
        id = getArguments().getString(KEY_ID);

        presenter.getPackagePeriodDetails(id);
        return view;
    }

    @Override
    public void populateView(PackagePeriodDetails packagePeriodDetails) {
        lstPackageDetails.setAdapter(new PackagePeriodDetailsAdapter(getActivity(),packagePeriodDetails.getPackages(),packagePeriodDetails.getTitle()));
    }

    @Override
    public void showProgress(){
    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(),message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showOfflineMessage(boolean isCritical) {

    }
}
