package com.rayanmehr.ziyarati.echarge.tools;

import com.rayanmehr.ziyarati.echarge.db.DatabaseHelper;
import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.db.model.PackageDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackagePeriodDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PreNumber;
import com.rayanmehr.ziyarati.echarge.model.ResponseOperator;
import com.rayanmehr.ziyarati.echarge.model.ResponsePackageDetails;
import com.rayanmehr.ziyarati.echarge.model.ResponsePackagePeriodDetails;
import com.rayanmehr.ziyarati.echarge.model.ResponsePackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.model.ResponsePreNumber;

import java.sql.SQLException;
import java.util.ArrayList;


/**
 * Created by ziyarati on 2/25/2017.
 */

public class hardcodes {



    public hardcodes(DatabaseHelper databaseHelper) throws SQLException {
        Operator irancellOP = new Operator("ایرانسل", "1", "http://blog.carti.ir/wp-content/uploads/2013/02/logo-irancell.jpg");
        Operator mciOP =      new Operator("همراه اول", "2", "http://0912sim.ir/wp-content/uploads/2016/11/hamrahe-aval.png");
        Operator rightelOP =  new Operator("رایتل", "3", "http://img.wmobile.ir/rightel.jpg");

        ArrayList<Operator> lstOperators = new ArrayList<>();
        lstOperators.add(irancellOP);
        lstOperators.add(mciOP);
        lstOperators.add(rightelOP);


//        ArrayList<PreNumber> irancellPreNums = new ArrayList<>();
//        irancellPreNums.add(new PreNumber(irancellOP, "935"));
//        irancellPreNums.add(new PreNumber(irancellOP, "936"));
//        irancellPreNums.add(new PreNumber(irancellOP, "937"));
//        irancellPreNums.add(new PreNumber(irancellOP, "938"));
//        irancellPreNums.add(new PreNumber(irancellOP, "939"));
//        irancellPreNums.add(new PreNumber(irancellOP, "903"));
//
//        ArrayList<PreNumber> mciPreNums = new ArrayList<>();
//        mciPreNums.add(new PreNumber(mciOP, "910"));
//        mciPreNums.add(new PreNumber(mciOP, "919"));
//        mciPreNums.add(new PreNumber(mciOP, "911"));
//        mciPreNums.add(new PreNumber(mciOP, "912"));
//        mciPreNums.add(new PreNumber(mciOP, "913"));
//        mciPreNums.add(new PreNumber(mciOP, "914"));
//        mciPreNums.add(new PreNumber(mciOP, "915"));
//        mciPreNums.add(new PreNumber(mciOP, "916"));
//        mciPreNums.add(new PreNumber(mciOP, "917"));
//        mciPreNums.add(new PreNumber(mciOP, "918"));
//
//        ArrayList<PreNumber> rightelPreNums = new ArrayList<>();
//        rightelPreNums.add(new PreNumber(rightelOP, "921"));
//        rightelPreNums.add(new PreNumber(rightelOP, "922"));





        ArrayList<PackageTypeDetails> packageTypeDetails = new ArrayList();

        packageTypeDetails.add(new PackageTypeDetails(irancellOP, "سیم کارت اعتباری", "1"));
        packageTypeDetails.add(new PackageTypeDetails(irancellOP, "سیم کارت دائمی", "2"));
        packageTypeDetails.add(new PackageTypeDetails(irancellOP, "بسته اینترنت 4G", "3"));


        packageTypeDetails.add(new PackageTypeDetails(mciOP, "سیم کارت اعتباری", "4"));
        packageTypeDetails.add(new PackageTypeDetails(mciOP, "سیم کارت دائمی", "5"));
        packageTypeDetails.add(new PackageTypeDetails(mciOP, "بسته اینترنت 4G", "6"));

        packageTypeDetails.add(new PackageTypeDetails(rightelOP, "سیم کارت اعتباری", "7"));
        packageTypeDetails.add(new PackageTypeDetails(rightelOP, "سیم کارت دائمی", "8"));
        packageTypeDetails.add(new PackageTypeDetails(rightelOP, "بسته اینترنت 4G", "9"));




        ArrayList<PackagePeriodDetails> packagePeriodDetailses = new ArrayList<>();
//        for (ResponsePackageTypeDetails packageTypeDetail:
//                packageTypeDetails)
        ArrayList<PackageDetails> lstPackages1 = new ArrayList<>();

        for (int i = 0; i < packageTypeDetails.size(); i++)

        {
            packagePeriodDetailses.add(new PackagePeriodDetails(packageTypeDetails.get(i), "شگفت انگیز", i * 7 + 0 + "", 9));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"40 مگابایت", "سه روزه", i * 16 + 0 + "", "10000", "بسته شگفت انگیز 10000 ریالی"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"100 مگابایت", "پنج روزه", i * 16 + 1 + "", "10000", "بسته شگفت انگیز 10000 ریالی"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"270 مگابایت", "ده روزه", i * 16 + 2 + "", "10000", "بسته شگفت انگیز 10000 ریالی"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"600 مگابایت", "پانزده روزه", i * 16 + 3 + "", "10000", "بسته شگفت انگیز 10000 ریالی"));

            packagePeriodDetailses.add(new PackagePeriodDetails(packageTypeDetails.get(i), "ساعتی", i * 7 + 1 + "", 9));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"یک گیگابایت", "سه ساعته", i * 16 + 4 + "", "10000", "بسته اینترنت اعتباری - 2 تا 8 صبح"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"نا محدود", "پنج ساعته", i * 16 + 5 + "", "10000", "بسته اینترنت اعتباری - 2 تا 8 صبح"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"یک گیگابایت", "ده ساعته", i * 16 + 6 + "", "10000", "بسته اینترنت اعتباری - 2 تا 8 صبح"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"دو گیگابایت", "پانزده ساعته", i * 16 + 7 + "", "10000", "بسته اینترنت اعتباری - 2 تا 8 صبح"));

            packagePeriodDetailses.add(new PackagePeriodDetails(packageTypeDetails.get(i), "روزانه", i * 7 + 2 + "", 9));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"600 مگابایت", "سه روزه", i * 16 + 8 + "", "10000", "بسته اینترنت اعتباری - روزانه - 600 مگابایت"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"نا محدود", "پنج روزه", i * 16 + 9 + "", "10000", "بسته اینترنت اعتباری - روزانه - نامحدود "));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"یک گیگابایت", "ده روزه", i * 16 + 10 + "", "10000", "بسته اینترنت اعتباری - روزانه - یک گیگابایت"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"دو گیگابایت", "پانزده روزه", i * 16 + 11 + "", "10000", "بسته اینترنت اعتباری - روزانه - دو گیگابایت"));

            packagePeriodDetailses.add(new PackagePeriodDetails(packageTypeDetails.get(i), "هفتگی", i * 7 + 3 + "", 9));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"نا محدود", "سه هفته", i * 16 + 12 + "", "10000", "بسته اینترنت اعتباری - هفتگی - نا محدود - 2 تا 8 صبح"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"نا محدود", "پنج هفته", i * 16 + 13 + "", "10000", "بسته اینترنت اعتباری - هفتگی - نا محدود - 2 تا 8 صبح"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"یک گیگابایت", "ده هفته", i * 16 + 14 + "", "10000", "بسته اینترنت اعتباری - هفتگی - یک گیگابایت"));
            lstPackages1.add(new PackageDetails(packagePeriodDetailses.get(packagePeriodDetailses.size()-1),"دو گیگابایت", "پانزده هفته",i * 16 + 15 + "", "10000", "بسته اینترنت اعتباری - هفتگی - دو گیگابایت "));


            packagePeriodDetailses.add(new PackagePeriodDetails(packageTypeDetails.get(i), "پانزده روزه", i * 7 + 4 + "", 9));
            packagePeriodDetailses.add(new PackagePeriodDetails(packageTypeDetails.get(i), "ماهانه", i * 7 + 5 + "", 9));
            packagePeriodDetailses.add(new PackagePeriodDetails(packageTypeDetails.get(i), "دو ماهه", i * 7 + 6 + "", 9));
        }

//        databaseHelper.saveOperators(lstOperators);
//
//        databaseHelper.savePreNumbers(irancellPreNums);
//        databaseHelper.savePreNumbers(mciPreNums);
//        databaseHelper.savePreNumbers(rightelPreNums);
//        databaseHelper.savePackageTypes(packageTypeDetails);
//
//        databaseHelper.savePackagePeriods(packagePeriodDetailses);
//
//        databaseHelper.savePackageDetails(lstPackages1);


        ArrayList<ResponseOperator> lstOP = new ArrayList<ResponseOperator>();
        for (Operator op:
             databaseHelper.getOperators()) {
            ArrayList<ResponsePackageTypeDetails> ptd1 =
                    new ArrayList<ResponsePackageTypeDetails>();
            for (PackageTypeDetails ptd :
                    op.getPackageTypes()) {
                ArrayList<ResponsePackagePeriodDetails> ppd1 =
                        new ArrayList<ResponsePackagePeriodDetails>();
                for (PackagePeriodDetails ppd:
                     ptd.getPackagePeriods()) {

                    ArrayList<ResponsePackageDetails> pd1 = new ArrayList<ResponsePackageDetails>();
                    for (PackageDetails pd:
                         ppd.getPackages()) {
                        pd1.add(new ResponsePackageDetails(pd.getPackageSize(),pd.getPackageDescription(),pd.getUid(),pd.getPrice(),pd.getPackageDescription()));
                    }
                    ppd1.add(new ResponsePackagePeriodDetails(ppd.getTitle(),ppd.getUid(),pd1,ppd.getTaxPercent()));
                }
                ptd1.add(new ResponsePackageTypeDetails(ptd.getTitle(),ptd.getUid(),ppd1));
            }

            ArrayList<ResponsePreNumber> lstPns = new ArrayList<ResponsePreNumber>();
            for (PreNumber pn:
                 op.getPreNumbers()) {
                lstPns.add(new ResponsePreNumber(pn.getNumber(),pn.getId()+""));
            }
            lstOP.add(new ResponseOperator(op.getTitle(),op.getUid(),op.getImageUrl(),lstPns,ptd1));

        }



//        ArrayList<PackageType> lstPackTypes = new ArrayList<>();
//        lstPackTypes.add(new PackageType("سیم کارت اعتباری", "1"));
//        lstPackTypes.add(new PackageType("سیم کارت دائمی", "2"));
//        lstPackTypes.add(new PackageType("بسته اینترنت 4G", "3"));

        String str = lstOP.toString();


        ////////////////////////////////
        ////////////////////////////////
        ////////////////////////////////

//        ArrayList<PackagePeriod> lstPackPeriods = new ArrayList<>();
//        lstPackPeriods.add(new PackagePeriod("شگفت انگیز", "1"));
//        lstPackPeriods.add(new PackagePeriod("ساعتی", "2"));
//        lstPackPeriods.add(new PackagePeriod("روزانه", "3"));
//        lstPackPeriods.add(new PackagePeriod("هفتگی", "4"));
//        lstPackPeriods.add(new PackagePeriod("پانزده روزه", "5"));
//        lstPackPeriods.add(new PackagePeriod("ماهانه", "6"));
//        lstPackPeriods.add(new PackagePeriod("دو ماهه", "7"));
//        lstPackPeriods.add(new PackagePeriod("سه ماهه", "8"));
//        lstPackPeriods.add(new PackagePeriod("شش ماهه", "9"));
//        lstPackPeriods.add(new PackagePeriod("یک ساله", "10"));
//
//
//        PackageType packageType1 = new PackageType("سیم کارت اعتباری", "1");
//        PackageType packageType2 = new PackageType("سیم کارت دائمی", "2");
//        PackageType packageType3 = new PackageType("بسته اینترنت 4G", "3");

        ////////////////////////////////////////
        ///////////////////////////////////////
        ////////////////////////////////////////



    }
}
