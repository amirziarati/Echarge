package com.rayanmehr.ziyarati.echarge.operators;

import com.rayanmehr.ziyarati.echarge.client.EchargeApi;
import com.rayanmehr.ziyarati.echarge.model.ResponseOperator;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observable;

/**
 * Created by ziyarati on 2/23/2017.
 */
public class OperatorsInteractor implements OperatorsContract.Interactor{

    EchargeApi api;

    @Inject
    public OperatorsInteractor(EchargeApi api) {
        this.api = api;
    }

    @Override
    public Observable<ArrayList<ResponseOperator>> getOperators() {
        return api.getOperator();
    }

}
