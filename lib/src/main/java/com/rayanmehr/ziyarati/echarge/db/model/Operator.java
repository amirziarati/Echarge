package com.rayanmehr.ziyarati.echarge.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class Operator extends ModelBase {
    @DatabaseField
    private String title;
    @DatabaseField(id = true)
    private String uid;
    @DatabaseField
    private String imageUrl;
    @ForeignCollectionField(eager = false)
    private ForeignCollection<PreNumber> preNumbers;
    @ForeignCollectionField(eager = false)
    private ForeignCollection<PackageTypeDetails> packageTypes;

    public Operator() {
    }

    public Operator(String title, String uid, String imageUrl) {
        this.title = title;
        this.uid = uid;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<PreNumber> getPreNumbers() {
        if(preNumbers == null)
            return null;
        return new ArrayList<PreNumber>(preNumbers);
    }

    public void setPreNumbers(ForeignCollection<PreNumber> preNumbers) {
        this.preNumbers = preNumbers;
    }

    public ArrayList<PackageTypeDetails> getPackageTypes() {
        if(packageTypes == null)
            return null;
        return new ArrayList<PackageTypeDetails>(packageTypes);
    }

    public void setPackageTypes(ForeignCollection<PackageTypeDetails> packageTypes) {
        this.packageTypes = packageTypes;
    }
}
