package com.rayanmehr.ziyarati.echarge.base;

/**
 * Created by ziyarati on 2/23/2017.
 */
public interface BaseView {

    void showMessage(String message);

    void showOfflineMessage(boolean isCritical);

}
