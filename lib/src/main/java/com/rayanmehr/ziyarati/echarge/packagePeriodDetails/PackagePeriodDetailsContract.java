package com.rayanmehr.ziyarati.echarge.packagePeriodDetails;

import com.rayanmehr.ziyarati.echarge.base.BasePresenter;
import com.rayanmehr.ziyarati.echarge.base.BaseView;
import com.rayanmehr.ziyarati.echarge.db.model.PackagePeriodDetails;

/**
 * Created by ziyarati on 2/23/2017.
 */
interface PackagePeriodDetailsContract {

    interface View extends BaseView {


        void populateView(PackagePeriodDetails packagePeriodDetails);

        void showProgress();

        void hideProgress();

    }

    interface Presenter extends BasePresenter<View> {

        void getPackagePeriodDetails(String packagePeriodId);

    }

}
