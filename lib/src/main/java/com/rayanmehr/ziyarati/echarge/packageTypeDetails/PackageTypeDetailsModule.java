package com.rayanmehr.ziyarati.echarge.packageTypeDetails;


import dagger.Module;
import dagger.Provides;

/**
 * Created by ziyarati on 2/23/2017.
 */
@Module
public class PackageTypeDetailsModule {


    PackageTypeDetailsContract.View view;
    public PackageTypeDetailsModule(PackageTypeDetailsContract.View view)
    {
        this.view = view;
    }


    @Provides
    public PackageTypeDetailsContract.Presenter providePresenter(PackageTypeDetailsPresenter presenter) {
        presenter.bind(view);
        return presenter;
    }

}
