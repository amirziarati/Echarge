package com.rayanmehr.ziyarati.echarge.operators;

import com.rayanmehr.ziyarati.echarge.base.BasePresenter;
import com.rayanmehr.ziyarati.echarge.base.BaseView;
import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.model.ResponseOperator;

import java.util.ArrayList;

import rx.Observable;

/**
 * Created by ziyarati on 2/23/2017.
 */
public interface OperatorsContract {

    interface View extends BaseView {


        void populateView(ArrayList<Operator> operator);

        void showProgress();

        void hideProgress();

    }

    interface Presenter extends BasePresenter<View> {

        void getAllOperators();

    }

    interface Interactor {

        Observable<ArrayList<ResponseOperator>> getOperators();
    }

}
