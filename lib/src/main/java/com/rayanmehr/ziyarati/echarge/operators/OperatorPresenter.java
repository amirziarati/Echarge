package com.rayanmehr.ziyarati.echarge.operators;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.rayanmehr.ziyarati.echarge.SchedulerProvider;
import com.rayanmehr.ziyarati.echarge.StateManager;
import com.rayanmehr.ziyarati.echarge.db.DatabaseHelper;
import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.db.model.PackageDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackagePeriodDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PreNumber;
import com.rayanmehr.ziyarati.echarge.model.ResponseOperator;
import com.rayanmehr.ziyarati.echarge.model.ResponsePackageDetails;
import com.rayanmehr.ziyarati.echarge.model.ResponsePackagePeriodDetails;
import com.rayanmehr.ziyarati.echarge.model.ResponsePackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.model.ResponsePreNumber;

import java.sql.SQLException;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Named;

import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ziyarati on 2/23/2017.
 */
public class OperatorPresenter implements OperatorsContract.Presenter {

    OperatorsInteractor operatorsInteractor;
    OperatorsContract.View view;
    CompositeSubscription subscriptions;
    SchedulerProvider schedulerProvider;
    StateManager stateManager;
    DatabaseHelper databaseHelper;

    @Inject
    public OperatorPresenter(SchedulerProvider sp, OperatorsInteractor someInteractor, StateManager stateManager, DatabaseHelper databaseHelper, @Named("defaultResponse") String defaultResponse) {
        this.schedulerProvider = sp;
        this.operatorsInteractor = someInteractor;
        subscriptions = new CompositeSubscription();
        this.stateManager = stateManager;
        this.databaseHelper = databaseHelper;
        try {
            if (databaseHelper.getOperators().size() == 0)
                saveOperators((ArrayList<ResponseOperator>) (new Gson()).fromJson(defaultResponse, new TypeToken<ArrayList<ResponseOperator>>() {}.getType()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getAllOperators() {
        if (null != view) {
            view.showProgress();
        }
        if (!stateManager.isConnect()) {
            sendOperatorsFromDBtoView();
            view.hideProgress();
        } else {
            subscriptions.add(
                    operatorsInteractor.getOperators()
                            .subscribeOn(schedulerProvider.backgroundThread())
                            .observeOn(schedulerProvider.mainThread())
                            .subscribe(new Action1<ArrayList<ResponseOperator>>() {
                                @Override
                                public void call(ArrayList<ResponseOperator> operators) {
                                    if (view != null) {
                                        //save response in DB
                                        try {
                                            saveOperators(operators);
                                        } catch (SQLException e) {
                                            e.printStackTrace();
                                        }
                                        //get operators from DB and return
                                        sendOperatorsFromDBtoView();
                                        view.hideProgress();

                                    }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                    if (view != null) {
                                        view.hideProgress();
                                        sendOperatorsFromDBtoView();
                                    }
                                }
                            })
            );
        }
    }

    private void sendOperatorsFromDBtoView() {
        try {
            view.populateView(databaseHelper.getOperators());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void saveOperators(ArrayList<ResponseOperator> responseOperators) throws SQLException {
        databaseHelper.recreateAllTables();
        ArrayList<Operator> arrOp = new ArrayList<Operator>();
        for (ResponseOperator responseOperator :
                responseOperators) {
            Operator resultOperator = new Operator(responseOperator.getTitle(), responseOperator.getUid(), responseOperator.getImageUrl());
            databaseHelper.saveOperator(resultOperator);
            if (responseOperator.getPackageTypes() != null) {
                for (ResponsePackageTypeDetails pd :
                        responseOperator.getPackageTypes()) {
                    PackageTypeDetails ptd = new PackageTypeDetails(resultOperator, pd.getTitle(), pd.getUid());
                    databaseHelper.savePackageType(ptd);
                    if (pd.getPackagePeriods() != null) {
                        for (ResponsePackagePeriodDetails rppd :
                                pd.getPackagePeriods()) {
                            PackagePeriodDetails ppd = new PackagePeriodDetails(ptd, rppd.getTitle(), rppd.getUid(), rppd.getTaxPercent());
                            databaseHelper.savePackagePeriod(ppd);
                            if (rppd.getPackages() != null) {
                                for (ResponsePackageDetails rpd :
                                        rppd.getPackages()) {
                                    databaseHelper.savePackageDetail(new PackageDetails(ppd, rpd.getPackageSize(), rpd.getPackageDuration(), rpd.getUid(), rpd.getPrice(), rpd.getPackageDescription()));
                                }
                            }
                        }
                    }
                }
            }
            if (responseOperator.getPreNumbers() != null) {
                for (ResponsePreNumber pn :
                        responseOperator.getPreNumbers()) {
                    databaseHelper.savePreNumber(new PreNumber(resultOperator, pn.getId(), pn.getNumber()));
                }
            }
        }
    }

    @Override
    public void bind(OperatorsContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        this.view = null;
        this.operatorsInteractor = null;
        subscriptions.clear();
    }
}
