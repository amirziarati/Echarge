package com.rayanmehr.ziyarati.echarge.model;

import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class ResponsePackagePeriodDetails extends ResponseModelBase {
    private String title;
    private String uid;
    private ArrayList<ResponsePackageDetails> packages;
    private int taxPercent;


    public ResponsePackagePeriodDetails(String title, String uid, ArrayList<ResponsePackageDetails> packages, int taxPercent) {
        this.title = title;
        this.uid = uid;
        this.packages = packages;
        this.taxPercent = taxPercent;
    }

    public int getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(int taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<ResponsePackageDetails> getPackages() {
        return packages;
    }

    public void setPackages(ArrayList<ResponsePackageDetails> packages) {
        this.packages = packages;
    }



}
