package com.rayanmehr.ziyarati.echarge.packagePeriodDetails;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ziyarati on 2/23/2017.
 */
@Module
public class PackagePeriodDetailsModule {

    PackagePeriodDetailsContract.View view;
    public PackagePeriodDetailsModule(PackagePeriodDetailsContract.View view)
    {
        this.view = view;
    }

    @Provides
    public PackagePeriodDetailsContract.Presenter providePresenter(PackagePeriodDetailsPresenter presenter) {
        presenter.bind(view);
        return presenter;
    }

}
