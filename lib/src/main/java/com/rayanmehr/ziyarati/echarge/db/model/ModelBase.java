package com.rayanmehr.ziyarati.echarge.db.model;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class ModelBase  implements Serializable {
    @Override
    public String toString() {
        return  (new Gson()).toJson(this);
    }
}
