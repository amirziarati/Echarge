package com.rayanmehr.ziyarati.echarge.db.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by amir on 3/1/17.
 */
public class PreNumber extends ModelBase {
    @DatabaseField(id = true)
    private String id;
    @DatabaseField
    private
    String number;
    @DatabaseField(foreign = true)
    private
    Operator operator;

    public PreNumber(){}

    public PreNumber(Operator operator,String id,String number) {
        this.operator = operator;
        this.number = number;
        this.id = id;
    }



    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Operator getOperator() {
        return operator;
    }
}
