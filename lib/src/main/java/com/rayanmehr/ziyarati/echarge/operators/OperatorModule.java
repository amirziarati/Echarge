package com.rayanmehr.ziyarati.echarge.operators;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ziyarati on 2/23/2017.
 */
@Module
public class OperatorModule {

    OperatorsContract.View view;
    public OperatorModule(OperatorsContract.View view)
    {
        this.view = view;
    }

    @Provides
    public OperatorsContract.Interactor provideInteractor(OperatorsInteractor interactor) {
        return interactor;
    }

    @Provides
    public OperatorsContract.Presenter providePresenter(OperatorPresenter presenter) {
        presenter.bind(view);
        return presenter;
    }

}
