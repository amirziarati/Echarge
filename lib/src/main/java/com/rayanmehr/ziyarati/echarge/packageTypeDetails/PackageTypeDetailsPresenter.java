package com.rayanmehr.ziyarati.echarge.packageTypeDetails;

import com.rayanmehr.ziyarati.echarge.db.DatabaseHelper;

import java.sql.SQLException;

import javax.inject.Inject;

/**
 * Created by ziyarati on 2/23/2017.
 */
public class PackageTypeDetailsPresenter implements PackageTypeDetailsContract.Presenter {

    PackageTypeDetailsContract.View view;
    DatabaseHelper databaseHelper;

    @Inject
    public PackageTypeDetailsPresenter(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @Override
    public void getPackageTypeDetails(String packageTypeId) {
        if (null != view) {
            view.showProgress();
            try {
                view.populateView(databaseHelper.getPackageTypeByID(packageTypeId));
            } catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                view.hideProgress();
            }
        }

    }

    @Override
    public void bind(PackageTypeDetailsContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        this.view = null;
    }
}
