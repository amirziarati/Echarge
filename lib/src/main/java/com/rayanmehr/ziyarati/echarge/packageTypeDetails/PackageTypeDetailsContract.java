package com.rayanmehr.ziyarati.echarge.packageTypeDetails;

import com.rayanmehr.ziyarati.echarge.base.BasePresenter;
import com.rayanmehr.ziyarati.echarge.base.BaseView;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;

/**
 * Created by ziyarati on 2/23/2017.
 */
interface PackageTypeDetailsContract {

    interface View extends BaseView {


        void populateView(PackageTypeDetails packageTypeDetails);

        void showProgress();

        void hideProgress();

    }

    interface Presenter extends BasePresenter<View> {

        void getPackageTypeDetails(String packageTypeId);

    }



}
