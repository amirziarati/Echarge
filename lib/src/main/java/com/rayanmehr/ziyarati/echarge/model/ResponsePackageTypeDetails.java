package com.rayanmehr.ziyarati.echarge.model;

import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class ResponsePackageTypeDetails extends ResponseModelBase {
    private String title;
    private String uid;
    private ArrayList<ResponsePackagePeriodDetails> packagePeriods;

    public ResponsePackageTypeDetails(String title, String uid, ArrayList<ResponsePackagePeriodDetails> packagePeriods) {
        this.title = title;
        this.uid = uid;
        this.packagePeriods = packagePeriods;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<ResponsePackagePeriodDetails> getPackagePeriods() {
        return packagePeriods;
    }

    public void setPackagePeriods(ArrayList<ResponsePackagePeriodDetails> packagePeriods) {
        this.packagePeriods = packagePeriods;
    }
}
