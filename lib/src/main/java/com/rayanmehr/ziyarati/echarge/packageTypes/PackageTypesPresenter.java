package com.rayanmehr.ziyarati.echarge.packageTypes;

import com.rayanmehr.ziyarati.echarge.db.DatabaseHelper;

import java.sql.SQLException;

import javax.inject.Inject;

/**
 * Created by ziyarati on 2/23/2017.
 */
public class PackageTypesPresenter implements PackageTypesContract.Presenter {

    PackageTypesContract.View view;
    DatabaseHelper databaseHelper;

    @Inject
    public PackageTypesPresenter(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @Override
    public void getPackageTypes(String operatorId) {
        if (null != view) {
            view.showProgress();
            try {
                view.populateView(databaseHelper.getOperatorByID(operatorId).getPackageTypes());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            finally {
                view.hideProgress();
            }

        }
    }


    @Override
    public void bind(PackageTypesContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        this.view = null;
    }
}
