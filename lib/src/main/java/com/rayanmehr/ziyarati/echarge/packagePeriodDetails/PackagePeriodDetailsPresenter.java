package com.rayanmehr.ziyarati.echarge.packagePeriodDetails;

import com.rayanmehr.ziyarati.echarge.db.DatabaseHelper;

import java.sql.SQLException;

import javax.inject.Inject;

/**
 * Created by ziyarati on 2/23/2017.
 */
public class PackagePeriodDetailsPresenter implements PackagePeriodDetailsContract.Presenter {

    PackagePeriodDetailsContract.View view;
    DatabaseHelper databaseHelper;

    @Inject
    public PackagePeriodDetailsPresenter(DatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
    }

    @Override
    public void getPackagePeriodDetails(String packagePeriodId) {
        if (null != view) {
            view.showProgress();
            try {
                view.populateView(databaseHelper.getPackagePeriodByID(packagePeriodId));
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                view.hideProgress();
            }
        }

    }

    @Override
    public void bind(PackagePeriodDetailsContract.View view) {
        this.view = view;
    }

    @Override
    public void unbind() {
        this.view = null;

    }
}
