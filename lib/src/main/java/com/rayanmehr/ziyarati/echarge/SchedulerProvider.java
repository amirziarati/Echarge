package com.rayanmehr.ziyarati.echarge;

import rx.Scheduler;

/**
 * Created by ziyarati on 2/23/2017.
 */

public interface SchedulerProvider {

    Scheduler mainThread();

    Scheduler backgroundThread();

}
