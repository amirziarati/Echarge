package com.rayanmehr.ziyarati.echarge.packageTypes;


import dagger.Module;
import dagger.Provides;

/**
 * Created by ziyarati on 2/23/2017.
 */
@Module
public class PackageTypesModule {


    PackageTypesContract.View view;
    public PackageTypesModule(PackageTypesContract.View view)
    {
        this.view = view;
    }

    @Provides
    public PackageTypesContract.Presenter providePresenter(PackageTypesPresenter presenter) {
        presenter.bind(view);
        return presenter;
    }

}
