package com.rayanmehr.ziyarati.echarge.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class PackageTypeDetails extends ModelBase {
    @DatabaseField
    private String title;
    @DatabaseField(id = true)
    private String uid;
    @ForeignCollectionField(eager = false)
    private ForeignCollection<PackagePeriodDetails> packagePeriods;
    @DatabaseField(foreign = true)
    private
    Operator operator;

    public PackageTypeDetails(){}

    public PackageTypeDetails(Operator operator,String title, String uid) {
        this.setOperator(operator);
        this.title = title;
        this.uid = uid;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<PackagePeriodDetails> getPackagePeriods() {
        if(packagePeriods == null)
            return null;
        return new ArrayList<PackagePeriodDetails>(packagePeriods);
    }

    public void setPackagePeriods(ForeignCollection<PackagePeriodDetails> packagePeriods) {
        this.packagePeriods = packagePeriods;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }
}
