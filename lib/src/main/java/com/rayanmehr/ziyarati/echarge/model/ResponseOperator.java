package com.rayanmehr.ziyarati.echarge.model;

import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class ResponseOperator extends ResponseModelBase {
    private String title;
    private String uid;
    private String imageUrl;
    private ArrayList<ResponsePreNumber> preNumbers;
    private ArrayList<ResponsePackageTypeDetails> packageTypes;

    public ResponseOperator(String title, String uid, String imageUrl, ArrayList<ResponsePreNumber> preNumbers, ArrayList<ResponsePackageTypeDetails> packageTypes) {
        this.title = title;
        this.uid = uid;
        this.imageUrl = imageUrl;
        this.preNumbers = preNumbers;
        this.packageTypes = packageTypes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public ArrayList<ResponsePreNumber> getPreNumbers() {
        return preNumbers;
    }

    public void setPreNumbers(ArrayList<ResponsePreNumber> preNumbers) {
        this.preNumbers = preNumbers;
    }

    public ArrayList<ResponsePackageTypeDetails> getPackageTypes() {
        return packageTypes;
    }

    public void setPackageTypes(ArrayList<ResponsePackageTypeDetails> packageTypes) {
        this.packageTypes = packageTypes;
    }
}
