package com.rayanmehr.ziyarati.echarge.model;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class ResponsePackageDetails extends ResponseModelBase {
    private String packageSize;
    private String packageDuration;
    private String packageDescription;
    private String uid;
    private String price;


    public ResponsePackageDetails(String packageSize, String duration, String uid, String price, String packageDescription) {
        this.packageSize = packageSize;
        this.packageDuration = duration;
        this.uid = uid;
        this.price = price;
        this.packageDescription = packageDescription;
    }



    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getPackageSize() {
        return packageSize;
    }

    public void setPackageSize(String packageSize) {
        this.packageSize = packageSize;
    }

    public String getPackageDuration() {
        return packageDuration;
    }

    public void setPackageDuration(String packageDuration) {
        this.packageDuration = packageDuration;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
