package com.rayanmehr.ziyarati.echarge.model;

import com.google.gson.Gson;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class ResponseModelBase {
    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }
}
