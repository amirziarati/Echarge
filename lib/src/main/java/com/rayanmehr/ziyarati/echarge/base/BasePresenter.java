package com.rayanmehr.ziyarati.echarge.base;

/**
 * Created by ziyarati on 2/23/2017.
 */
public interface BasePresenter<T> {

    void bind(T view);

    void unbind();

}
