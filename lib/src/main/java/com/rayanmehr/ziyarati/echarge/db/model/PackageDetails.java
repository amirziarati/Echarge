package com.rayanmehr.ziyarati.echarge.db.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class PackageDetails extends ModelBase {
    @DatabaseField
    private String packageSize;
    @DatabaseField
    private String packageDuration;
    @DatabaseField
    private String packageDescription;
    @DatabaseField(id = true)
    private String uid;
    @DatabaseField
    private String price;
    @DatabaseField(foreign = true,maxForeignAutoRefreshLevel = 5,foreignAutoRefresh = true)
    private
    PackagePeriodDetails packagePeriodDetails;

    public PackageDetails()
    {

    }

    public PackageDetails(PackagePeriodDetails packagePeriodDetails,String packageSize, String duration, String uid, String price,String packageDescription) {
        this.setPackagePeriodDetails(packagePeriodDetails);
        this.packageSize = packageSize;
        this.packageDuration = duration;
        this.uid = uid;
        this.price = price;
        this.packageDescription = packageDescription;
    }




    public String getPackageDescription() {
        return packageDescription;
    }

    public void setPackageDescription(String packageDescription) {
        this.packageDescription = packageDescription;
    }

    public String getPackageSize() {
        return packageSize;
    }

    public void setPackageSize(String packageSize) {
        this.packageSize = packageSize;
    }

    public String getPackageDuration() {
        return packageDuration;
    }

    public void setPackageDuration(String packageDuration) {
        this.packageDuration = packageDuration;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public PackagePeriodDetails getPackagePeriodDetails() {
        return packagePeriodDetails;
    }

    public void setPackagePeriodDetails(PackagePeriodDetails packagePeriodDetails) {
        this.packagePeriodDetails = packagePeriodDetails;
    }
}
