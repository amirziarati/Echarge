package com.rayanmehr.ziyarati.echarge.packageTypes;

import com.rayanmehr.ziyarati.echarge.base.BasePresenter;
import com.rayanmehr.ziyarati.echarge.base.BaseView;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;

import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */
interface PackageTypesContract {

    interface View extends BaseView {


        void populateView(ArrayList<PackageTypeDetails> packageTypes);

        void showProgress();

        void hideProgress();

    }

    interface Presenter extends BasePresenter<View> {

        void getPackageTypes(String operatorId);

    }


}
