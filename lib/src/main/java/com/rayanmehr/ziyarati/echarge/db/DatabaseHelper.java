package com.rayanmehr.ziyarati.echarge.db;


import com.rayanmehr.ziyarati.echarge.db.model.Operator;
import com.rayanmehr.ziyarati.echarge.db.model.PackageDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackagePeriodDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PackageTypeDetails;
import com.rayanmehr.ziyarati.echarge.db.model.PreNumber;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */

public interface DatabaseHelper {

    void saveOperator(Operator operator) throws SQLException;
    void savePreNumber(PreNumber preNumber) throws SQLException;
    void savePackageType(PackageTypeDetails typesDetail) throws SQLException;
    void savePackagePeriod(PackagePeriodDetails periodsDetail) throws SQLException;
    void savePackageDetail(PackageDetails packagesDetail) throws SQLException;

    void recreateAllTables() throws SQLException;

    ArrayList<Operator>  getOperators() throws SQLException;
    Operator  getOperatorByID(String id) throws SQLException;
    ArrayList<PackageTypeDetails>  getPackageTypes() throws SQLException;
    PackageTypeDetails getPackageTypeByID(String id) throws SQLException;
    ArrayList<PackagePeriodDetails>  getPackagePeriods() throws SQLException;
    PackagePeriodDetails  getPackagePeriodByID(String id) throws SQLException;
    ArrayList<PackageDetails>  getPackageDetails() throws SQLException;
}
