package com.rayanmehr.ziyarati.echarge.db.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import java.util.ArrayList;

/**
 * Created by ziyarati on 2/23/2017.
 */

public class PackagePeriodDetails extends ModelBase {
    @DatabaseField
    private String title;
    @DatabaseField(id = true)
    private String uid;
    @ForeignCollectionField(eager = false)
    private ForeignCollection<PackageDetails> packages;
    @DatabaseField
    private int taxPercent;
    @DatabaseField(foreign = true,maxForeignAutoRefreshLevel = 4,foreignAutoRefresh = true)
    private
    PackageTypeDetails packageTypeDetails;

    public PackagePeriodDetails()
    {

    }

    public PackagePeriodDetails(PackageTypeDetails packageTypeDetails,String title, String uid, int taxPercent) {
        this.setPackageTypeDetails(packageTypeDetails);
        this.title = title;
        this.uid = uid;
        this.taxPercent = taxPercent;
    }

    public int getTaxPercent() {
        return taxPercent;
    }

    public void setTaxPercent(int taxPercent) {
        this.taxPercent = taxPercent;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public ArrayList<PackageDetails> getPackages() {
        if(packages == null)
            return null;
        return new ArrayList<PackageDetails>(packages);

    }

    public void setPackages(ForeignCollection<PackageDetails> packages) {
        this.packages = packages;
    }


    public PackageTypeDetails getPackageTypeDetails() {
        return packageTypeDetails;
    }

    public void setPackageTypeDetails(PackageTypeDetails packageTypeDetails) {
        this.packageTypeDetails = packageTypeDetails;
    }
}
