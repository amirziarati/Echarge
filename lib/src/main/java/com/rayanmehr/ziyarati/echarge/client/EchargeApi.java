package com.rayanmehr.ziyarati.echarge.client;



import com.rayanmehr.ziyarati.echarge.model.ResponseOperator;

import java.util.ArrayList;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by ziyarati on 2/23/2017.
 */

public interface EchargeApi {


    @GET("/operators/all.json")
    Observable<ArrayList<ResponseOperator>> getOperator();

//    @GET("/GetPackageTypes/{uid}.json")
//    Observable<ArrayList<PackageType>> getOperatorPackageTypes(@Path("uid") String operatorID);
//
//    @GET("/GetPackageTypeDetails/{uid}.json")
//    Observable<ResponsePackageTypeDetails> getPackageTypeDetail(@Path("uid") String packageTypeId);
//
//
//    @GET("/GetPackagePeriodDetails/{uid}.json")
//    Observable<ResponsePackagePeriodDetails> getPackagePeriodDetail(@Path("uid") String packagePeriodId);

}
