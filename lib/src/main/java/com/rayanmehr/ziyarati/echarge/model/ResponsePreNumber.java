package com.rayanmehr.ziyarati.echarge.model;

import com.rayanmehr.ziyarati.echarge.db.model.ModelBase;

/**
 * Created by amir on 3/1/17.
 */
public class ResponsePreNumber extends ModelBase {
    private String id;
    private String number;


    public ResponsePreNumber(String number, String id) {
        this.setNumber(number);
        this.setId(id);
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
